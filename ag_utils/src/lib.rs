//! utility node definitions used by the more complex examples.

use async_graph::*;
use core::marker::PhantomData;

pub struct Add<T>{
    ty: PhantomData<T>,
    name: String,
}
impl<T> Add<T>{
    pub fn new(name: &str) -> Add<T>{
	Add{
	    ty: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Add<T> where T: core::ops::Add<Output = T>, T: Send + 'static{
    type Inputs = (T, T);
    type Outputs = [T; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	[input.0 + input.1]
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

pub struct Mul<T>{
    ty: PhantomData<T>,
    name: String,
}
impl<T> Mul<T>{
    pub fn new(name: &str) -> Mul<T>{
	Mul{
	    ty: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Mul<T> where T: core::ops::Mul<Output = T>, T: Send + 'static{
    type Inputs = (T, T);
    type Outputs = [T; 1];

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	[input.0 * input.1]
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

///Duplicates any incoming data into two
pub struct Dup<T>{
    ty: PhantomData<T>,
    name: String,
}
impl<T> Dup<T>{
    pub fn new(name: &str) -> Dup<T>{
	Dup{
	    ty: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Dup<T> where T: Send + Clone + 'static{
    type Inputs = [T; 1];
    type Outputs = (T, T);

    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	(input[0].clone(), input[0].clone())
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}

///The Receiver node just prints some value.
pub struct Receiver<T>{
    t: PhantomData<T>,
    name: String,
}
impl<T> Receiver<T>{
    pub fn new(name: &str) -> Self{
	Receiver{
	    t: PhantomData,
	    name: name.to_owned(),
	}
    }
}
impl<T> Node for Receiver<T> where T: std::fmt::Debug + Send + 'static{
    type Inputs = [T;1];
    type Outputs = ();

    fn process(&self, input: Self::Inputs) -> Self::Outputs {
	println!("Receiver: {}: {:?}", self.name, input[0]);
    }

    fn debug_info(&self) -> &str {
	&self.name
    }
}
