/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::collections::HashMap;
use crate::*;

use crate::log::error;

///A bridge is only defined by the count in/outputs. The number of in and outputs is always
/// the same. The type of this is determined by the receiving node. If a value with a wrong type is send,
/// similar to the nodes `WrongDatatype` is returned to the provider.
///
///The number of input ids can dynamically grow or shrink based on the `set_out_edge` and `remove_out_edge` calls.
/// `set_in` calls on indexes without a set receiver node return `OutIndexNotInUse`.
pub struct Bridge{
    //Mapping input_index -> (target_node_idx, target_node)
    mapping: HashMap<usize, (usize, Arc<RwLock<dyn AbstAggregator + Send + Sync>>)>,
    name: String,
}

impl Bridge{
    pub fn new(name: Option<String>) -> Self{
	Bridge{
	    mapping: HashMap::new(),
	    name: name.unwrap_or(String::from("unnamed bridge"))
	}
    }
}

impl AbstAggregator for Bridge{
    fn set_in(&mut self, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError>{
	if let Some((target_index, pot_target)) = self.mapping.get(&index){
	    pot_target.write().unwrap().set_in(*target_index, val)
	}else{
	    Err(GraphError::BridgeIndexNotSet(index, self.name.clone()))
	}	
    }

    fn set_out_edge(&mut self, edge: Edge) -> Result<(), GraphError>{
	//If there is already some receiver set, return error.
	if self.mapping.contains_key(&edge.out_idx){
	    return Err(GraphError::OutIndexInUse(edge.out_idx));
	}

	let old = self.mapping.insert(edge.out_idx, (edge.in_idx, edge.target));
	debug_assert!(old.is_none(), "There should not be a target present while inserting in a bridge!");

	Ok(())
    }

    fn remove_out_edge(&mut self, index: usize) -> Result<(), GraphError>{
	if !self.mapping.contains_key(&index){
	    Err(GraphError::BridgeIndexNotSet(index, self.name.clone()))
	}else{
	    let old = self.mapping.remove(&index);
	    debug_assert!(old.is_some(), "When removing an edge from a bridge, the removed key should have a value!");
	    Ok(())
	}
    }

    fn get_in_type_id(&self, index: usize) -> Result<TypeId, GraphError>{
	if let Some(edge) = self.mapping.get(&index){
	    edge.1.read().unwrap().get_in_type_id(edge.0)
	}else{
	    error!("No index for index {}", index);
	    Err(GraphError::BridgeIndexNotSet(index, self.name.clone()))
	}
    }

    fn get_out_type_id(&self, index: usize) -> Result<TypeId, GraphError>{
	if let Some(edge) = self.mapping.get(&index){
	    edge.1.read().unwrap().get_in_type_id(edge.0)
	}else{
	    error!("No index for index {}", index);
	    Err(GraphError::BridgeIndexNotSet(index, self.name.clone()))
	}
    }
}
