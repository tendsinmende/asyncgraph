use serde::{Serialize, Deserialize};
use crate::*;

#[derive(Serialize, Deserialize)]
struct SerBridge{}

#[derive(Serialize, Deserialize)]
struct SerNode{}

#[derive(Serialize, Deserialize)]
struct SerGraph{}

///Defines the serialized object
#[derive(Serialize, Deserialize)]
enum Object{
    Bridge(SerBridge),
    Node(SerNode),
    Graph(SerGraph)
}

