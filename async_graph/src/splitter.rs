/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::*;
use crate::log::warn;
//---SPLITTERS----

///Special implementation for nodes that do not output anything.
impl Splitter for (){
    type DataLayout = ();
    fn set_receiver(&mut self, _edge: Edge) -> Result<(), GraphError>{
	Err(GraphError::NoOutput)
    }

    fn remove_edge(&mut self, _index: usize) -> Result<(), GraphError> {
	Err(GraphError::NoOutput)
    }
    
    fn split(&self, _data: Self::DataLayout, _node_info: &str) -> Result<(), GraphError>{
	Err(GraphError::NoOutPorts)
    }
}

///Implementation for a single value `A`. Currently wraps it into the concrete type `[A;1]` since specialization is not yet implemented.
/// That's why the trait can not be implemented for `DataLayout=A` since this would shadow every other implementation.
impl<A> Splitter for SplitRec<A> where A: Send + 'static{
    type DataLayout = [A; 1];
    fn set_receiver(&mut self, edge: Edge) -> Result<(), GraphError>{

	if edge.out_idx != 0{
	    return Err(GraphError::ExceedsIndex(0));
	}

	if self.receiver.is_some(){
	    return Err(GraphError::OutIndexInUse(0));
	}

	let receiver_type_id = edge.target.read()
	    .unwrap()
	    .get_in_type_id(edge.in_idx)?;

	if receiver_type_id != TypeId::of::<A>(){
	    return Err(GraphError::DataTypeMissmatch{
		type_at_from: TypeId::of::<A>(),
		type_at_to: receiver_type_id,
	    });
	}
	
	self.receiver = Some(edge.target);
	self.target_index = edge.in_idx;
	self.type_id = receiver_type_id;

	Ok(())
    }

    fn remove_edge(&mut self, index: usize) -> Result<(), GraphError> {
	if index == 0{
	    self.receiver = None;
	    Ok(())
	}else{
	    Err(GraphError::ExceedsIndex(0))
	}
    }
    
    fn split(&self, data: Self::DataLayout, node_info: &str) -> Result<(), GraphError>{
	//TODO: resolve when RFC:1210 (https://github.com/rust-lang/rfcs/blob/master/text/1210-impl-specialization.md)
	// has landed
	
	let [d] = data; 
	
	if let Some(rec) = &self.receiver{
	    let mut write_gurad = rec.write().unwrap();
	    write_gurad.set_in(self.target_index, Box::new(d))?;
	    Ok(())
	}else{
	    warn!("No receiver set for node: {}", node_info);
	    //Not set but okay
	    Ok(())
	}	
    }
}

//All tuple implementations
impl_splitter!(A:0, B:1, 1);
impl_splitter!(A:0, B:1, C:2, 2);
impl_splitter!(A:0, B:1, C:2, D:3, 3);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, 4);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, 5);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, 6);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, 7);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, 8);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, 9);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, 10);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, 11);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, 12);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, 13);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, 14);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, 15);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, 16);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, 17);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, 18);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, 19);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, 20);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, 21);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, 22);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, 23);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, 24);
impl_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, Z:25, 25);
//---IntoSplitter---

impl IntoSplitter for (){
    type SplitterType = ();
    fn to_splitter() -> Self::SplitterType {
	
    }
    fn get_type_id(_index: usize) -> Result<TypeId, GraphError>{
	Err(GraphError::ExceedsIndex(0))
    }
}

///Implementation for a single value `A`. Currently wraps it into the concrete type `[A;1]` since specialization is not yet implemented.
/// That's why the trait can not be implemented for `A` since this would shadow every other implementation.
impl<A> IntoSplitter for [A; 1] where A: Send + 'static{
    type SplitterType = SplitRec<A>;
    fn to_splitter() -> Self::SplitterType {
	SplitRec{
	    receiver: None,
	    type_id: TypeId::of::<()>(),
	    target_index: 0,
	    phantom: PhantomData,
	}
    }
    fn get_type_id(index: usize) -> Result<TypeId, GraphError>{
	if index == 0{
	    Ok(TypeId::of::<A>())
	}else{
	    Err(GraphError::ExceedsIndex(0))
	}
    }
}

impl_into_splitter!(A:0, B:1, 1);
impl_into_splitter!(A:0, B:1, C:2, 2);
impl_into_splitter!(A:0, B:1, C:2, D:3, 3);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, 4);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, 5);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, 6);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, 7);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, 8);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, 9);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, 10);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, 11);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, 12);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, 13);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, 14);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, 15);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, 16);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, 17);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, 18);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, 19);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, 20);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, 21);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, 22);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, 23);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, 24);
impl_into_splitter!(A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, I:8, J:9, K:10, L:11, M:12, N:13, O:14, P:15, Q:16, R:17, S:18, T:19, U:20, V:21, W:22, X:23, Y:24, Z:25, 25);


///Implements `Splitter` for any tupel like this: `(A:0, B:1, C:2, 2)` etc. The first character is the generics name, the second should be a incrementing number, which is the generics location within the resulting tupel. The last is the max index that can be reached. 
#[macro_export]
macro_rules! impl_splitter {
    ( $( $Gen:ident : $Idx:tt ),+, $Max:literal ) => {
        impl<$($Gen),+> Splitter for ($(SplitRec<$Gen>),+) where $($Gen: Send + 'static),+{
	    type DataLayout = ($($Gen),+);
	    fn set_receiver(&mut self, edge: Edge) -> Result<(), GraphError>{

		match edge.out_idx{
		    $(
			$Idx => {
			    if self.$Idx.receiver.is_some(){
				return Err(GraphError::OutIndexInUse($Idx));
			    }
			    
			    let receiver_type_id = edge.target.read()
				.unwrap()
				.get_in_type_id(edge.in_idx)?;

			    if receiver_type_id != TypeId::of::<$Gen>(){
				return Err(GraphError::DataTypeMissmatch{
				    type_at_from: TypeId::of::<$Gen>(),
				    type_at_to: receiver_type_id,
				});
			    }
			    
			    self.$Idx.receiver = Some(edge.target);
			    self.$Idx.target_index = edge.in_idx;
			    self.$Idx.type_id = receiver_type_id;
			    Ok(())
			}
		    ),+
			//Default impl for exceeding
			_ => {
			    Err(GraphError::ExceedsIndex($Max)) //TODO fix 
			}	
		}
	    }

	    fn remove_edge(&mut self, index: usize) -> Result<(), GraphError> {
		match index{
		    $(
			$Idx => {
			    self.$Idx.receiver = None;
			    Ok(())
			}
		    ),+
		    _ => Err(GraphError::ExceedsIndex($Max))
		}
	    }
	    
	    fn split(&self, data: Self::DataLayout, node_info: &str) -> Result<(), GraphError>{

		$(
		    if self.$Idx.receiver.is_none(){
			warn!("No receiver set for output={} of node: {}", $Idx, node_info);
		    }
		)+

		$(
		    if let Some(rec) = &self.$Idx.receiver{
			let mut write_gurad = rec.write().unwrap();
			write_gurad.set_in(self.$Idx.target_index, Box::new(data.$Idx))?;
		    }else{
			warn!("node {} has not receiver for {}", node_info, $Idx);
		    }
		)+
		Ok(())
	    }
	}
    };
}



///Implemets the into_splitter trait based on the same input as the impl_splitter macro.
#[macro_export]
macro_rules! impl_into_splitter {
    ( $( $Gen:ident : $Idx:literal ),+, $Max:literal ) => {
        impl<$($Gen),+> IntoSplitter for ($($Gen),+) where $($Gen : Send + 'static),+ {
	    type SplitterType = ($(SplitRec<$Gen>),+);
	    fn to_splitter() -> Self::SplitterType{
		($(
		    SplitRec{
			receiver: None,
			type_id: TypeId::of::<$Gen>(),
			target_index: 0,
			phantom: PhantomData,
		    }
		),+)
	    }

	    fn get_type_id(index: usize) -> Result<TypeId, GraphError>{
		match index{
		    $(
			$Idx =>{
			    Ok(TypeId::of::<$Gen>())
			}
		    ),+
		    _ => Err(GraphError::ExceedsIndex($Max))
		}
	    }
	}
    };
}
