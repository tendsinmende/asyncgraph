/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::*;
use std::sync::{Arc, RwLock};
use std::collections::HashMap;

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct NodeId{
    graph: usize,
    node: usize,
}

///Abstract graph object that can be serialized and de-serialized.
///
///They are also quiet convenient to store sub graph in. Kind of like function in programming languages.
pub struct Graph{

    ///Inner graph id
    id: usize,

    ///Routing the incoming edges
    in_bridge: Arc<RwLock<Bridge>>,

    ///Routing the outgoing edges
    out_bridge: Arc<RwLock<Bridge>>,
    
    ///All nodes in this graph
    nodes: HashMap<NodeId, Arc<RwLock<dyn AbstAggregator + Send + Sync>>>,

    next_id: usize,
}

impl Graph{

    pub fn new(id: usize) -> Self{
	Graph{
	    id,
	    in_bridge: Arc::new(RwLock::new(Bridge::new(Some(format!("Graph({}) in bridge", id))))),
	    out_bridge: Arc::new(RwLock::new(Bridge::new(Some(format!("Graph({}) out bridge", id))))),
	    nodes: HashMap::new(),
	    next_id: 0,
	}
    }
    
    ///Connects the in_idx of this graph to some nodes in_idx
    pub fn connect_in_to_node(&mut self, graph_in_idx: usize, node: &NodeId, node_in_idx: usize) -> Result<(), GraphError>{
	let node_ref = if let Some(n) = self.get_node(node){
	    n
	}else{
	    return Err(GraphError::NodeNotFound);
	};

	self.in_bridge.write().unwrap().set_out_edge(Edge{
	    out_idx: graph_in_idx,
	    in_idx: node_in_idx,
	    target: node_ref,
	})?;

	Ok(())
    }

    ///Connects the out_idx of some node to the out_idx of this graph.
    pub fn connect_node_to_out(&mut self, node: &NodeId, node_out_idx: usize, out_idx: usize) -> Result<(), GraphError>{
	let node_ref = if let Some(n) = self.get_node(node){
	    n
	}else{
	    return Err(GraphError::NodeNotFound);
	};
	node_ref.write().unwrap().set_out_edge(Edge{
	    out_idx: node_out_idx,
	    in_idx: out_idx,
	    target: self.out_bridge.clone(),  
	})?;
	    
	Ok(())
    }

    pub fn add_node<N>(&mut self, node: N) -> NodeId
    where N: Node + Send + Sync + 'static,
    <<N as Node>::Inputs as IntoInputCollector>::Collector: Send + Sync,
    <<N as Node>::Outputs as IntoSplitter>::SplitterType: Sync,
    <N as Node>::Outputs: Send + 'static,
    <N as Node>::Inputs: Send + 'static  

    {
	let new_id = NodeId{
	    graph: self.id,
	    node: self.next_id
	};
	self.next_id += 1;

	self.nodes.insert(
	    new_id,
	    Aggregator::from_node(node)
	);

	new_id
    }

    pub fn add_arc(&mut self, node: Arc<RwLock<dyn AbstAggregator + Send + Sync>>) -> NodeId
    {
	let new_id = NodeId{
	    graph: self.id,
	    node: self.next_id
	};
	self.next_id += 1;

	self.nodes.insert(
	    new_id,
	    node
	);

	new_id
    }

    pub fn get_node(&self, id: &NodeId) -> Option<Arc<RwLock<dyn AbstAggregator + Send + Sync>>>{
	self.nodes.get(id).cloned()
    }
}


impl AbstAggregator for Graph{
    fn set_in(&mut self, index: usize, val: Box<dyn Any + Send>) -> Result<(), GraphError>{
	self.in_bridge.write().unwrap().set_in(index, val)
    }
    
    fn set_out_edge(&mut self, edge: Edge) -> Result<(), GraphError>{
	self.out_bridge.write().unwrap().set_out_edge(edge)
    }

    fn remove_out_edge(&mut self, index: usize) -> Result<(), GraphError>{
	self.out_bridge.write().unwrap().remove_out_edge(index)
    }
    
    fn get_in_type_id(&self, index: usize) -> Result<TypeId, GraphError>{
	self.in_bridge.read().unwrap().get_in_type_id(index)
    }

    fn get_out_type_id(&self, index: usize) -> Result<TypeId, GraphError>{
	self.out_bridge.read().unwrap().get_out_type_id(index)
    }
}
