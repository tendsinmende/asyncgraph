//! Example that shows how to setup a graph object and serialize it to disk.
use std::sync::{Arc, RwLock};
use ag_utils::*;
use async_graph::*;
use futures::future;
use std::thread;

fn main(){    
    //Create the graph object
    let graph = Arc::new(RwLock::new(Graph::new(1337)));

    //Add some nodes.
    let add_1 = graph.write().unwrap().add_node(Add::new("add 1") as Add<f32>);
    let add_2 = graph.write().unwrap().add_node(Add::new("add 2") as Add<f32>);
    let mul_1 = graph.write().unwrap().add_node(Mul::new("mul 1") as Mul<f32>);
    let mul_2 = graph.write().unwrap().add_node(Mul::new("mul 2") as Mul<f32>);
    let dup_1 = graph.write().unwrap().add_node(Dup::new("duplicate 1") as Dup<f32>);
    let dup_2 = graph.write().unwrap().add_node(Dup::new("duplicate 2") as Dup<f32>);

    //receiver nodes that are "outside" of this graph.
    let reciver_1 = Aggregator::from_node(Receiver::new("receiver 1") as Receiver<f32>);
    let reciver_2 = Aggregator::from_node(Receiver::new("receiver 2") as Receiver<f32>);
    
    //Connect nodes in a way that we get the graph
    // GRAPH(1337)                                  outside of 1337
    // __________________________________________
    // |     6               12         24      |
    //0|---|Add1|----------|Mul1|-----|Mul2|----|0---|receiver 1|
    //1|--/                /         /          |
    // |     2            /         /   2       |
    //2|---|Add2|--|dup1|/----|dup2|------------|1---|receiver 2|
    //3|--/                                     |
    // |________________________________________|
    //

    graph.write().unwrap().set_out_edge(Edge{ out_idx: 0, in_idx: 0, target: reciver_1.clone()}).unwrap();
    graph.write().unwrap().set_out_edge(Edge{ out_idx: 1, in_idx: 0, target: reciver_2.clone()}).unwrap();
    
    //Set in going connections
    graph.write().unwrap().connect_in_to_node(0, &add_1, 0).unwrap();
    graph.write().unwrap().connect_in_to_node(1, &add_1, 1).unwrap();

    graph.write().unwrap().connect_in_to_node(2, &add_2, 0).unwrap();
    graph.write().unwrap().connect_in_to_node(3, &add_2, 1).unwrap();
    //Set outgoing connections
    graph.write().unwrap().connect_node_to_out(&mul_2, 0, 0).unwrap();
    graph.write().unwrap().connect_node_to_out(&dup_2, 1, 1).unwrap();

    //Set in graph connections
    graph.read().unwrap().get_node(&add_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&mul_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&mul_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&mul_2).unwrap()}).unwrap();

    graph.read().unwrap().get_node(&add_2).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&dup_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: graph.read().unwrap().get_node(&mul_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 0, target: graph.read().unwrap().get_node(&dup_2).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_2).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: graph.read().unwrap().get_node(&mul_2).unwrap()}).unwrap();
    println!("Start graph with input");
    //Push some data into the graph
    smol::run(async {
	graph.write().unwrap().set_in(0 as usize, Box::new(4.0 as f32)).unwrap();
	graph.write().unwrap().set_in(1 as usize, Box::new(2.0 as f32)).unwrap();
	graph.write().unwrap().set_in(2 as usize, Box::new(1.0 as f32)).unwrap();
	graph.write().unwrap().set_in(3 as usize, Box::new(1.0 as f32)).unwrap();
    });
}
