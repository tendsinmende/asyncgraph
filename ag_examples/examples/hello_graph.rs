/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use async_graph::*;
use futures::future;
use std::thread;

///This example show how to implement a basic node that print "hello graph" every time it is pinged.
///While this makes no sense from a computational point, it shows how to define a simple node and define
///what happens if the node is executed.


///Every node needs some data type which defines the node it self. This data-type needs to implement `Node` to be usable as a node within a graph.
///However, the type can also be used in other contexts, for instance as a widget in some UI toolkit.
struct HelloGraph;

///This is where the nodes behavior upon calling gets implemented. There are three main parts that we need to define:
///
/// - Which input takes the node
/// - Which output is produced by the node
/// - What does the node do.
///
/// The In/Outputs are defined as types. The types need to implement the `IntoInput` trait when used as input, and the `IntoSplitter` trait when
/// used as output. Both traits are implemented for every tuple of the pattern (A,B) - (A, B, .., Z). So in/Outputs with up to 26 ins/outs are already
/// taken care of. The Generics A,B,...,Z can be any data type that implements `Send`.
///
/// There is a special case for single in/outputs. Those are defined as `[A;1]`. We can't implement the traits on a generic `A`, since this would shadow every special
/// implementation like `(A,B)` which is a subset of `A`. However, this won't be necessary when specialization lands in the rust compiler.
///
/// Last but not least, the `process()` function. Its where the actual magic happen. It gets supplied the `Inputs` we defined and needs to produces something with the
/// type of `Outputs` we defined. There are no other requirements. So do whatever you want :D. In out case the input is just some `bool` that tells us "ey something you should execute".
/// In that case we print "hello graph" and do nothing else, since the output type is nothing.
///
/// for a more complex example have a look at `math.rs`.

impl Node for HelloGraph{
    type Inputs = [bool;1];
    type Outputs = ();

    fn process(&self, _input: Self::Inputs) -> Self::Outputs{	
	println!("hello graph");
    }
}


///Most of the stuff you see here is boilerplate. We setup a async runtime of `smol` and build the graph.
/// Then, within the async block, the graph (with only our node inside) is supplied the "ping" bool. Again,
/// there is a more complex graph (with several nodes) in math.rs.
fn main(){

    //Start a thread pool
    let num_threads = 16;

    // Run the thread-local and work-stealing executor on a thread pool.
    for _ in 0..num_threads {
	// A pending future is one that simply yields forever.
	thread::spawn(|| smol::run(future::pending::<()>()));
    }

    //Setup nodes
    let hello_node = HelloGraph;
    let hello_aggregator = Aggregator::from_node(hello_node);

    //Push some data into the graph
    smol::run(async {
	hello_aggregator.write().unwrap().set_in(0 as usize, Box::new(true)).unwrap();
    });
}
