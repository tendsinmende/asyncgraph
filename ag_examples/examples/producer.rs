//! Copy of the Graph example, but this the graph gets its input periodically from two producers instead of the
//! manual `set_in` calls.
use std::sync::{Arc, RwLock};
use ag_utils::*;
use async_graph::*;
use futures::future;
use std::thread;

use simple_logger;

struct FloatProducer{
    //All channels we have to send on
    channel: Vec<Arc<RwLock<dyn Splitter<DataLayout = (f32, f32)> + Send + Sync>>>,
    one: f32,
    two: f32,
}

impl ProducerAble for FloatProducer{
    type OutputLayout = (f32, f32);
    fn receiver_channel(&mut self, channel: Arc<RwLock<dyn Splitter<DataLayout = Self::OutputLayout> + Send + Sync>>){
	println!("Got channel!");
	self.channel.push(channel);
    }

    fn produce(&self) {
	for c in self.channel.iter(){
	    smol::run(async{
		if let Err(e) = c.read().unwrap().split((self.one, self.two), "SomeProd"){
		    println!("Graph was not ready for product!");
		}
	    })
	}
    }
}

impl Node for FloatProducer{
    type Inputs = ();
    type Outputs = (f32, f32);
    fn process(&self, input: Self::Inputs) -> Self::Outputs{
	(self.one, self.two)
    }
}


fn main(){

    //simple_logger::init().expect("Failed to init simple logger");
    
    //Create the graph object
    let graph = Arc::new(RwLock::new(Graph::new(1337)));

    //Create producer, but keep a copy here, that we'll use in two different thread to periodically send some output
    let prod1 = Arc::new(RwLock::new(FloatProducer{channel: vec![], one: 4.0, two: 3.0}));
    let prod2 = Arc::new(RwLock::new(FloatProducer{channel: vec![], one: 5.0, two: 5.0}));

    //Now add producer reference to graph
    let prod1_id = graph.write().unwrap().add_arc(Aggregator::from_arc_producer(prod1.clone()));
    let prod2_id = graph.write().unwrap().add_arc(Aggregator::from_arc_producer(prod2.clone()));
    
    //Add some nodes.
    let add_1 = graph.write().unwrap().add_node(Add::new("add 1") as Add<f32>);
    let add_2 = graph.write().unwrap().add_node(Add::new("add 2") as Add<f32>);
    let mul_1 = graph.write().unwrap().add_node(Mul::new("mul 1") as Mul<f32>);
    let mul_2 = graph.write().unwrap().add_node(Mul::new("mul 2") as Mul<f32>);
    let dup_1 = graph.write().unwrap().add_node(Dup::new("duplicate 1") as Dup<f32>);
    let dup_2 = graph.write().unwrap().add_node(Dup::new("duplicate 2") as Dup<f32>);

    //receiver nodes that are "outside" of this graph.
    let reciver_1 = Aggregator::from_node(Receiver::new("receiver 1") as Receiver<f32>);
    let reciver_2 = Aggregator::from_node(Receiver::new("receiver 2") as Receiver<f32>);
    
    //Connect nodes in a way that we get the graph
    // GRAPH(1337)                                         outside of 1337
    // ________________________________________________
    // |                                              |
    // ||prod|---|Add1|----------|Mul1|-----|Mul2|----|0---|receiver 1|
    // ||  1 |--/                /         /          |
    // |                        /         /           |
    // ||prod|---|Add2|--|dup1|/----|dup2|------------|1---|receiver 2|
    // ||  2 |--/                                     |
    // |______________________________________________|
    //

    //Set out where the graphs out goes
    graph.write().unwrap().set_out_edge(Edge{ out_idx: 0, in_idx: 0, target: reciver_1.clone()}).unwrap();
    graph.write().unwrap().set_out_edge(Edge{ out_idx: 1, in_idx: 0, target: reciver_2.clone()}).unwrap();
    //Set which node connects to the graphs outs
    graph.write().unwrap().connect_node_to_out(&mul_2, 0, 0).unwrap();
    graph.write().unwrap().connect_node_to_out(&dup_2, 1, 1).unwrap();
    println!("--->");
    //Set connection from producers
    graph.read().unwrap().get_node(&prod1_id).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&add_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&prod1_id).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 1, target: graph.read().unwrap().get_node(&add_1).unwrap()}).unwrap();

    graph.read().unwrap().get_node(&prod2_id).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&add_2).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&prod2_id).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 1, target: graph.read().unwrap().get_node(&add_2).unwrap()}).unwrap();
    println!("<---");
    //Set in graph connections
    graph.read().unwrap().get_node(&add_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&mul_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&mul_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&mul_2).unwrap()}).unwrap();

    graph.read().unwrap().get_node(&add_2).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 0, target: graph.read().unwrap().get_node(&dup_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: graph.read().unwrap().get_node(&mul_1).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_1).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 1, in_idx: 0, target: graph.read().unwrap().get_node(&dup_2).unwrap()}).unwrap();
    graph.read().unwrap().get_node(&dup_2).unwrap().write().unwrap().set_out_edge(Edge{out_idx: 0, in_idx: 1, target: graph.read().unwrap().get_node(&mul_2).unwrap()}).unwrap();
    println!("Start graph with input");

    //Start two threads that push data into the graph periodically
    let _join1 = std::thread::spawn(move||{
	let start = std::time::Instant::now();
	while start.elapsed() < std::time::Duration::from_secs(30){
	    prod1.read().unwrap().produce();
	    std::thread::sleep(std::time::Duration::from_secs_f32(1.5));
	}
    });
    let _join2 = std::thread::spawn(move||{
	let start = std::time::Instant::now();
	while start.elapsed() < std::time::Duration::from_secs(30){
	    prod2.read().unwrap().produce();
	    std::thread::sleep(std::time::Duration::from_secs_f32(1.0));
	}
    });

    std::thread::sleep(std::time::Duration::from_secs(2));
}
